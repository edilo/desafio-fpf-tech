import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateVendaDto } from './dto/create-venda.dto';
import { Venda } from './entities/vendas.entity';
import { Produto } from '../produtos/entities/produtos.entity';
import { Cliente } from '../clientes/entities/clientes.entity';

@Injectable()
export class VendasService {
    //InjectRepository do proprio TypeOrm por padrão
    constructor(
        @InjectRepository(Venda)
        private vendasRepository: Repository<Venda>,
        @InjectRepository(Produto)
        private produtosRepository: Repository<Produto>
    ) { }

    async findOne(id: string): Promise<Venda> {
        return await this.vendasRepository.findOne({ where: { id: id } });
    }

    async findAll(): Promise<Venda[]> {
        return await this.vendasRepository.find();
    }

    async createVenda(data: CreateVendaDto) {
        const estoque = await this.consultarEstoque(data.fk_id_produto)
        if (data.quantidade > estoque.quantidade) {
            return { 'msg': 'Quantidade informada maior que a do estoque', 'qty': estoque.quantidade }
        }
        const result = await this.vendasRepository.save(data).then(data => data).catch((err) => err)
        if (result) {
            return { "Status": "ok", result }
        }
    }

    async consultarEstoque(id) {
        return await this.produtosRepository.findOne({ where: { id: id } });
    }

    async produtoMaisVendido() {
        const result = await this.vendasRepository
            .createQueryBuilder('vendas')
            .select('produto.descricao, fk_id_produto, COUNT(*) AS qtd')
            .innerJoin(Produto, "produto", "produto.id = vendas.fk_id_produto")
            .groupBy("fk_id_produto").getRawMany();
        return result
    }

    async clienteVendas() {
        const result = await this.vendasRepository
            .createQueryBuilder('vendas')
            .select('cliente.nome, fk_id_cliente, COUNT(*) AS qtd')
            .innerJoin(Cliente, "cliente", "cliente.id = vendas.fk_id_cliente")
            .groupBy("fk_id_cliente").getRawMany();
        return result
    }
}
