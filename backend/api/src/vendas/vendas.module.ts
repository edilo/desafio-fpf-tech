import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VendasController } from './vendas.controller';
import { VendasService } from './vendas.service';
import { Venda } from '../vendas/entities/vendas.entity';
import { Produto } from '../produtos/entities/produtos.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Venda]), TypeOrmModule.forFeature([Produto])],
    controllers: [VendasController],
    providers: [VendasService]
})
export class VendasModule { }
