import { IsInt, IsNotEmpty, IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class CreateVendaDto {
    @IsNumber()
    @Min(1, { message: 'O campo quantidade não pode ser 0' })
    @IsNotEmpty({ message: 'A quantidade não pode ser nula ou zero!' })
    quantidade: number;

    @IsNumber()
    @IsNotEmpty({ message: 'Escolha um cliente' })
    fk_id_cliente: number;

    @IsNumber()
    @IsNotEmpty({ message: 'Escolha um produto' })
    fk_id_produto: number;

    @IsString()
    updatedAt: string;

}

