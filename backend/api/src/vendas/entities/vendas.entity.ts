import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Venda extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @UpdateDateColumn({ name: 'updated_at', type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    updated_at: Date;

    @Column({ name: 'fk_id_cliente', type: 'int', nullable: false })
    fk_id_cliente: number;

    @Column({ name: 'fk_id_produto', type: 'int', nullable: false })
    fk_id_produto: number;

    @Column({ name: 'quantidade', type: 'int', nullable: true })
    quantidade: number;

   
}