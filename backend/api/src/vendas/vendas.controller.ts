import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { CreateVendaDto } from './dto/create-venda.dto';
import { VendasService } from './vendas.service';

@Controller('vendas')
export class VendasController {
  constructor(private readonly vendasService: VendasService) {}

  @Get()
  findOne(@Query('id') id: string) {
    return this.vendasService.findOne(id);
  }

  @Get('listarTodos')
  findAll() {
    return this.vendasService.findAll();
  }

  @Post()
  createVenda(@Body() venda: CreateVendaDto) {
    return this.vendasService.createVenda(venda);
  }

  @Get('/maisVendido')
  produtoMaisVendido() {
    return this.vendasService.produtoMaisVendido();
  }

  @Get('/clienteComprouMais')
  clienteVendas() {
    return this.vendasService.clienteVendas();
  }
}
