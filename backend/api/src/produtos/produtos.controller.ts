import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { CreateProdutoDto } from './dto/create-produto.dto';
import { ProdutosService } from './produtos.service'

@Controller('produtos')
export class ProdutosController {
    constructor(private readonly produtosService: ProdutosService) { }

    @Get()
    findOne(@Query('id') id: string) {
        return this.produtosService.findOne(id);
    }

    @Get('listarTodos')
    findAll() {
        return this.produtosService.findAll();
    }

    @Post()
    create(@Body() produto: CreateProdutoDto) {
        return this.produtosService.create(produto);
    }

    @Put()
    update(@Query('id') id: string, @Body() cliente: CreateProdutoDto) {
        return this.produtosService.update(id, cliente);
    }

    @Delete('/:id')
    delete(@Param('id') id: string) {
        return this.produtosService.delete(id);
    }
}
