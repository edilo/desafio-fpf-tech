import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProdutoDto } from './dto/create-produto.dto';
import { Produto } from './entities/produtos.entity';

@Injectable()
export class ProdutosService {
    //InjectRepository do proprio TypeOrm por padrão
    constructor(@InjectRepository(Produto) private produtosRepository: Repository<Produto>,) { }

    async findOne(id: string): Promise<Produto> {
        return await this.produtosRepository.findOne({ where: { id: id } });
    }

    async findAll(): Promise<Produto[]> {
        return await this.produtosRepository.find();
    }

    async create(data: CreateProdutoDto): Promise<Produto> {
        const result = await this.produtosRepository.save(data).then(data => data).catch((err) => err)

        if (result.updatedAt) {
            return result
        }
    }

    async update(id: string, data: CreateProdutoDto) {
        const result = await this.produtosRepository.createQueryBuilder()
            .update()
            .set(data)
            .where('id = :id', { id })
            .execute().then(data => data)
        if (result.affected > 0) {
            return { 'msg': `Produto com o ID ${id} alterado com sucesso` }
        } else {
            return { 'msg': `Produto com o ID ${id} não pode ser alterado` }
        }
    }

    async delete(id: string) {
        const result = await this.produtosRepository.createQueryBuilder()
            .delete()
            .from(Produto)
            .where("id = :id", { id })
            .execute().then(data => data)
        if (result.affected > 0) {
            return { 'msg': `Produto com o ID ${id} foi excluído com sucesso` }
        } else {
            return { 'msg': `Produto com o ID ${id} não pode ser excluido` }
        }

    }

}
