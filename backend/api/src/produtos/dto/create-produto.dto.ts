import { IsInt, IsNotEmpty, IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class CreateProdutoDto {
    @IsString()
    @IsNotEmpty({ message: 'O campo descrição não pode ser vazio!' })
    descricao: string;

    @IsOptional()
    @IsNumber()
    quantidade: number;

    @IsOptional()
    @IsNumber()
    preco: number;

}