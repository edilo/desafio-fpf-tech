import { BaseEntity, Column, Double, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Produto extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @UpdateDateColumn({ name: 'updated_at', type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    updatedAt: Date;

    @Column({ name: 'descricao', type: 'varchar', nullable: true })
    descricao: string;

    @Column({ name: 'quantidade', type: 'int', nullable: true })
    quantidade: number;

    @Column({ name: 'preco', type: 'float', nullable: true })
    preco: Double;

}