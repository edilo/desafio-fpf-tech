import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, Request } from '@nestjs/common';
import { CreateClienteDto } from './dto/create-cliente.dto';
import { ClientesService } from './clientes.service'

@Controller('clientes')
export class ClientesController {
    constructor(private readonly clientesService: ClientesService) { }

    @Get()
    findOne(@Query('id') id: string) {
        return this.clientesService.findOne(id);
    }

    @Get('listarTodos')
    findAll() {
        return this.clientesService.findAll();
    }

    @Post()
    create(@Body() cliente: CreateClienteDto) {
        return this.clientesService.create(cliente);
    }

    @Put()
    update(@Query('id') id: string, @Body() cliente: CreateClienteDto) {
        return this.clientesService.update(id, cliente);
    }

    @Delete('/:id')
    delete(@Param('id') id: string) {
        return this.clientesService.delete(id);
    }

}
