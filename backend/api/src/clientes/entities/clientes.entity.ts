import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Cliente extends BaseEntity {
 @PrimaryGeneratedColumn('increment')
 id: string;

 @UpdateDateColumn({ name: 'updated_at', type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
 updatedAt: Date;

 @Column({ name: 'nome', type: 'varchar',  nullable: false })
 nome: string;

 @Column({ name: 'rg', type: 'int', nullable: true })
 rg: number;

 @Column({ name: 'cpf', type: 'int', nullable: true })
 cpf: number;
}