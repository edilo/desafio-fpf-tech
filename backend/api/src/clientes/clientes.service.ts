import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateClienteDto } from './dto/create-cliente.dto';
import { Cliente } from './entities/clientes.entity';

@Injectable()
export class ClientesService {
    //InjectRepository do proprio TypeOrm por padrão
    constructor(@InjectRepository(Cliente) private clientesRepository: Repository<Cliente>,) { }

    async findAll(): Promise<Cliente[]> {
        return await this.clientesRepository.find();
    }

    async create(data: CreateClienteDto): Promise<Cliente> {
        const result = await this.clientesRepository.save(data).then(data => data).catch((err) => err)

        if (result.updatedAt) {
            return result
        }
    }

    async update(id: string, data: CreateClienteDto) {
        const result = await this.clientesRepository.createQueryBuilder()
            .update()
            .set(data)
            .where('id = :id', { id })
            .execute().then(data => data)
        if (result.affected > 0) {
            return { 'msg': `Cliente com o ID ${id} alterado com sucesso` }
        } else {
            return { 'msg': `Cliente com o ID ${id} não pode ser alterado` }
        }
    }

    async findOne(id: string): Promise<Cliente> {
        return await this.clientesRepository.findOne({ where: { id: id } });
    }

    async delete(id: string) {
        const result = await this.clientesRepository.createQueryBuilder()
            .delete()
            .from(Cliente)
            .where("id = :id", { id })
            .execute().then(data => data)
        if (result.affected > 0) {
            return { 'msg': `Cliente com o ID ${id} foi excluído com sucesso` }
        } else {
            return { 'msg': `Cliente com o ID ${id} não pode ser excluido` }
        }

    }

}
