import { IsInt, IsNotEmpty, IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class CreateClienteDto {
    @IsString()
    @IsNotEmpty({ message: 'O campo nome não pode ser vazio!' })
    nome: string;

    @IsOptional()
    @IsNumber()
    rg: number;

    @IsOptional()
    @IsNumber()
    cpf: number;

    //  @IsInt()
    //  @Min(0)
    //  quantity: number;
}