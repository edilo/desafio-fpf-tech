# Desafio fpf tech

Desafio para empresa FPFTECH - 2022

# Front-end
 - [x] App: Loja
 - [x] Função: Front-end desenvolvido para consumir uma API Rest com algumas funcionalidades de uma loja básica de qualquer seguimento.
 - [x] Stack - Angular CLI 13.3.3

 # Back-end
 - [x] App: Api
 - [x] Função: Back-end desenvolvido para disponibilizar rotas de acesso com retornos vindas do banco de dados para consumo de aplicações.
 - [x] Stack - Node js framework (Nest 8.2.2)
 - [x] **Dependências** :
   - [x] typeorm - 8.1.4 **Responsável por manipular nosso BD.**
   - [x] mysql2 - 2.3.3 **Banco de dados.**
   - [x] class-validator - 0.13.2  **Responsável por garantir que os dados recebidos estejam de acordo com as regras estabelecidas.**  
- [x] **Estrutura organizacional de pastas:**\
  ![](backend/API/util/images/Estrutura.png)
- [x] **Pasta Cliente e sua estrutura:**\
  ![](backend/API/util/images/cliente.png)