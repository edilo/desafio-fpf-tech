import {HttpClient} from '@angular/common/http'
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import Venda from '../../models/Venda.model'

@Injectable({
  providedIn: 'root'
})
export class VendaService {
  baseUrl: string = 'http://localhost:3000/vendas';

  constructor(private httpClient: HttpClient) { }

  listVenda() {
    return this.httpClient.get(`${this.baseUrl}/listartodos`)
    .pipe(
      map((resposta: any) => resposta)
    )
  }

  createVenda(venda: Venda){
    return this.httpClient.post(`${this.baseUrl}`, venda)
  }
}
