import {HttpClient} from '@angular/common/http'
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import Cliente from '../../models/Cliente.model'

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  baseUrl: string = 'http://localhost:3000/clientes';

  constructor(private httpClient: HttpClient) { }

  listCliente() {
    return this.httpClient.get(`${this.baseUrl}/listartodos`)
    .pipe(
      map((resposta: any) => resposta)
    )
  }

  createCliente(cliente: Cliente){
    return this.httpClient.post(`${this.baseUrl}`, cliente)
  }

  editCliente(cliente: Cliente){
    return this.httpClient.put(`${this.baseUrl}?id=${cliente.id}`, cliente)
  }

  deleteCliente(cliente: Number){
    return this.httpClient.delete(`${this.baseUrl}/${cliente}`)
  }
}
