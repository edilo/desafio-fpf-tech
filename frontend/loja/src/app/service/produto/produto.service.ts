import {HttpClient} from '@angular/common/http'
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import Produto from '../../models/Produto.model'

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {
  baseUrl: string = 'http://localhost:3000/produtos';

  constructor(private httpClient: HttpClient) { }

  listProduto() {
    return this.httpClient.get(`${this.baseUrl}/listartodos`)
    .pipe(
      map((resposta: any) => resposta)
    )
  }

  createProduto(produto: Produto){
    return this.httpClient.post(`${this.baseUrl}`, produto)
  }

  editProduto(produto: Produto){
    return this.httpClient.put(`${this.baseUrl}?id=${produto.id}`, produto)
  }

  deleteProduto(produto: Number){
    return this.httpClient.delete(`${this.baseUrl}/${produto}`)
  }

}
