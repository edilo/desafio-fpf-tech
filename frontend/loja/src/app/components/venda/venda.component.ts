import { Component, OnInit } from '@angular/core';
import Venda from '../../models/Venda.model';
import Cliente from '../../models/Cliente.model';
import Produto from '../../models/Produto.model';
import { VendaService } from '../../service/venda/venda.service'
import { ClienteService } from '../../service/cliente/cliente.service'
import { ProdutoService } from '../../service/produto/produto.service'

@Component({
  selector: 'app-venda',
  templateUrl: './venda.component.html'
})
export class VendaComponent implements OnInit {
  id = 0;
  fkidcliente = 0;
  fkidproduto = 0;
  quantidade = 0;
  status: boolean = false;

  vendas: Venda[] = [];
  clientes: Cliente[] = [];
  produtos: Produto[] = [];

  constructor(
              private vendaService: VendaService,
              private clienteService: ClienteService,
              private produtoService: ProdutoService
            ) { }

  ngOnInit(): void {
    this.listVenda()
    this.listarCliente()
    this.listarProduto()
  }

  listVenda(){
    return this.vendaService.listVenda().subscribe(response => {
       this.vendas = response
    })

  }

  listarCliente(){
    return this.clienteService.listCliente().subscribe(response => {
      this.clientes = response
   })
  }

  listarProduto(){
    return this.produtoService.listProduto().subscribe(response => {
      this.produtos = response
   })
  }

  onVender(): void {
    const venda: Venda = new Venda()
    venda.fk_id_cliente = this.fkidcliente
    venda.fk_id_produto = this.fkidproduto
    venda.quantidade = this.quantidade
    if(this.status === false){
        this.vendaService.createVenda(venda).subscribe(response => {
            alert("Venda Realizada com sucesso!")
            this.listVenda()
        })
    }
  }

}
