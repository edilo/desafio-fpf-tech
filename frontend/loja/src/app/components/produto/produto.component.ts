import { Component, OnInit } from '@angular/core';
import Produto from '../../models/Produto.model';
import { ProdutoService } from '../../service/produto/produto.service'

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html'
})
export class ProdutoComponent implements OnInit {
  id = 0;
  descricao: string = '';
  quantidade = 0;
  preco?: Number = 0;
  status: boolean = false;
  produtos: Produto[] = [];

  constructor(private produtoService: ProdutoService) { }

  ngOnInit(): void {
    this.listProduto()
  }

  listProduto(){
    return this.produtoService.listProduto().subscribe(response => {
       this.produtos = response
    })
    
  }

  onProduto(): void {
    const produto: Produto = new Produto()
    produto.descricao = this.descricao
    produto.quantidade = this.quantidade
    produto.preco = this.preco
    if(this.status === false){
      if(produto.descricao === '' || produto.descricao === null){
        alert('A descrição é obrigatória!')
      }else{
        this.produtoService.createProduto(produto).subscribe(response => {
            alert("Produto Cadastrado com sucesso!")
            this.listProduto()
            this.cleanFields()
        })
      }
    }else{
      produto.id = this.id
      if(produto.descricao === '' || produto.descricao === null){
        alert('A descrição é obrigatória!')
      }else{
          this.produtoService.editProduto(produto).subscribe(response => {
            alert("Produto Alterado com sucesso!")
            this.listProduto()
            this.cleanFields()
        })
      }
    }
  }

  deleteProd(id: Number) {
    this.produtoService.deleteProduto(id).subscribe(response => {
      alert("Produto Deletado com sucesso!")
      this.listProduto()
  })
  }

  showEdit(produto: Produto): void {
    this.status = true
    this.id = produto.id
    this.descricao = produto.descricao
    this.quantidade = produto.quantidade
    this.preco = produto.preco
  }

  cleanFields(){
    this.status = false
    this.descricao = ''
    this.quantidade = 0
    this.preco = 0
  }

}
