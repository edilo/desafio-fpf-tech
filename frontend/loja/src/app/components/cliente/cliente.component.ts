import { Component, OnInit } from '@angular/core';
import Cliente from '../../models/Cliente.model';
import { ClienteService } from '../../service/cliente/cliente.service'

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html'
})
export class ClienteComponent implements OnInit {
  id = 0;
  nome: string = '';
  rg = 0;
  cpf = 0;
  status: boolean = false;
  clientes: Cliente[] = [];

  constructor(private clienteService: ClienteService) { }

  ngOnInit(): void {
    this.listCliente()
  }

  listCliente(){
    return this.clienteService.listCliente().subscribe(response => {
       this.clientes = response
    })
    
  }

  onCliente(): void {
    const cliente: Cliente = new Cliente()
    cliente.nome = this.nome
    cliente.rg = this.rg
    cliente.cpf = this.cpf
    if(this.status === false){
      if(cliente.nome === '' || cliente.nome === null){
        alert('Nome do cliente obrigatório!')
      }else{
        this.clienteService.createCliente(cliente).subscribe(response => {
            alert("Cliente Cadastrado com sucesso!")
            this.listCliente()
            this.cleanFields()
        })
      }
    }else{
      cliente.id = this.id
      if(cliente.nome === '' || cliente.nome === null){
        alert('Nome do cliente obrigatório!')
      }else{
          this.clienteService.editCliente(cliente).subscribe(response => {
            alert("Cliente Alterado com sucesso!")
            this.listCliente()
            this.cleanFields()
        })
      }
    }
  }

  deleteCli(id: Number) {
    this.clienteService.deleteCliente(id).subscribe(response => {
      alert("Cliente Deletado com sucesso!")
      this.listCliente()
  })
  }

  showEdit(cliente: Cliente): void {
    this.status = true
    this.id = cliente.id
    this.nome = cliente.nome
    this.rg = cliente.rg
    this.cpf = cliente.cpf
  }

  cleanFields(){
    this.status = false
    this.nome = ''
    this.rg = 0
    this.cpf = 0
  }

}
