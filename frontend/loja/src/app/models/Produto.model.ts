export default class Produto {
    id: number = 0;
    descricao: string ='';
    quantidade = 0;
    preco?: Number = 0;
  }
  