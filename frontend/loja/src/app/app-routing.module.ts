import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../app/components/dashboard/dashboard.component'
import { ProdutoComponent } from '../app/components/produto/produto.component'
import { ClienteComponent } from '../app/components/cliente/cliente.component'
import { VendaComponent } from '../app/components/venda/venda.component'


const routes: Routes = [
  {path:'', component: DashboardComponent},
  {path:'produto', component: ProdutoComponent},
  {path:'cliente', component: ClienteComponent},
  {path:'venda', component: VendaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
